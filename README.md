**Client-Server Project**
This is a simple client-server application built with Java and Gradle.
The application allows clients to connect to a server and perform various operations such as user creation,
login, sending and reading emails, and retrieving server information.

**Getting Started**
These instructions will help you set up and run the project on your local machine.

**Prerequisites**
Java JDK 11 or higher
Gradle 7 or higher

**Installing**
1. Clone the repository:
   git clone https://gitlab.com/StanislawWegner/client_server.git

2. Navigate to the project directory:
   cd client_server

3. Build the project using Gradle:
   gradle build

**Running the Application**
Running the Server
To start the server, run the following command:
java -cp build/libs/Client_Server.jar org.example.server.Server

The server will listen for client connections on port 5000.

Running the Client
To start the client and connect to the server, run the following command:
java -cp build/libs/Client_Server.jar org.example.client.Client

The client will connect to the server running on localhost at port 5000.

**Usage**
Once the client is connected, you can use the following commands:

- help: Displays the list of available commands.
- create: Creates a new user account (requires admin privileges).
- login: Logs in with an existing user account.
- uptime: Displays the server's uptime.
- info: Displays the server version and creation date.
- user: Displays the currently logged-in user.
- users: Lists all users (requires admin privileges).
- delete: Deletes a user account (requires admin privileges).
- send mail: Sends an email to another user.
- read mail: Reads emails for the logged-in user.
- stop: Disconnects from the server.

**Project Structure**
The project is structured as follows:

- example/client: Contains the client-side classes.
- example/server: Contains the server-side classes.
- example/databaseConnection: Handles database connections.
- example/databaseService: Contains database-related services.
- example/mail: Handles email functionalities.
- example/serializeAndSend: Utility classes for serialization and communication.
- example/user: Handles user-related functionalities.

**Database Information**
The project originally used PostgreSQL for storing users and emails. As of version 5.0.0, the application uses SQLite for database storage. The PostgreSQL version of the project is still available on the PSQL branch for reference.

**Testing**
Unit tests are available for the database and mail services. To run the tests, use the following command:
gradle test

**Authors**
Stanisław Wegner

**License**
This project is licensed under the MIT License - see the LICENSE file for details.
