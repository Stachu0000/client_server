package org.example;

import org.example.utilities.CloseableUtil;
import org.junit.jupiter.api.Test;

import java.io.Closeable;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class CloseableUtilTest {

    @Test
    void shouldCloseResource_WhenResourceIsNotNull() throws IOException {
        //Given
        Closeable closeable = mock(Closeable.class);

        //When
        CloseableUtil.quietlyClose(closeable);

        //Then
        verify(closeable, times(1)).close();
    }

    @Test
    void shouldNotThrowException_WhenResourceIsNull(){
        //Given
        Closeable closeable = null;

        //When
        CloseableUtil.quietlyClose(closeable);

    }
}
