package org.example;

import org.example.entities.Mail;
import org.example.entities.service.MailService;
import org.junit.jupiter.api.*;

import java.util.Scanner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MailServiceTest {

    @Test
    void shouldReturnTrue_WhenMailLengthIsValid() {
        String body = "a".repeat(254);

        boolean result = MailService.validateMailLength(body);
        assertThat(result).isTrue();
    }

    @Test
    void shouldReturnFalse_WhenBodyIsTooLong() {
        String body = "a".repeat(256);

        boolean result = MailService.validateMailLength(body);
        assertThat(result).isFalse();
    }

    @Test
    void shouldValidateMailLength_WhenBodyLengthIsMax() {
        //Given
        String maxLengthBody = "a".repeat(255);

        //When
        boolean result = MailService.validateMailLength(maxLengthBody);

        //Then
        assertThat(result).isTrue();
    }

    @Test
    void shouldReturnTrue_WhenMailBodyIsEmpty(){
        //Given
        String emptyBody = "";

        //When
        boolean result = MailService.validateMailLength(emptyBody);

        //Then
        assertThat(result).isTrue();
    }

    @Nested
    class MailServiceWithMockScannerTest {
        Scanner mockScanner;
        MailService mailService;
        @BeforeEach
        void setUp() {
            mockScanner = mock(Scanner.class);
            mailService = new MailService(mockScanner);

        }

        @Test
        void shouldReturnNewMail_WhenInputIsValid() {
            //Given
            String sender = "sender";
            String body = "body";
            String recipient = "recipient";
            when(mockScanner.nextLine()).thenReturn(recipient, body);

            //When
            Mail mail = mailService.createMailFromUserInput(sender);

            //Then
            assertThat(mail).isNotNull();
            assertThat(mail.getSender()).isEqualTo(sender);
            assertThat(mail.getRecipient()).isEqualTo(recipient);
            assertThat(mail.getBody()).isEqualTo(body);
            assertThat(mail.isRead()).isFalse();
        }



    }
}