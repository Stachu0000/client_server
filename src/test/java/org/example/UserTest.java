package org.example;

import org.example.entities.Mail;
import org.example.entities.User;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class UserTest {

    @Test
    void shouldCreateUserWithNoArgsConstructor(){
        //When
        User user = new User();

        //Then
        assertNull(user.getLogin());
        assertNull(user.getPassword());
        assertNull(user.getType());
        assertEquals(new ArrayList<Mail>(), user.getMailbox());
    }

    @Test
    void shouldCreateUserWithAllArgsConstructor(){
        //Given
        String login = "login";
        String password = "password";
        User.Role role = User.Role.USER;

        //When
        User user = new User(login, password, role);

        //Then
        assertEquals(user.getLogin(), login);
        assertEquals(user.getPassword(), password);
        assertEquals(user.getRole(), role);
        assertEquals(user.getType(), "User");
        assertEquals(new ArrayList<Mail>(), user.getMailbox());
    }

    @Test
    void shouldGetAndSetLogin(){
        //Given
        String login = "login";
        User user = new User();

        //When
        user.setLogin(login);

        //Then
        assertThat(user.getLogin()).isEqualTo(login);
    }

    @Test
    void shouldGetAndSetPassword(){
        //Given
        String password = "password";
        User user = new User();

        //When
        user.setPassword(password);

        //Then
        assertThat(user.getPassword()).isEqualTo(password);
    }

    @Test
    void shouldGetAndSetRole(){
        //Given
        User.Role role = User.Role.USER;
        User user = new User();

        //When
        user.setRole(role);

        //Then
        assertThat(user.getRole()).isEqualTo(role);
    }

    @Test
    void shouldGetAndSetMailbox(){
        //Given
        User user = new User();
        Mail mail1 = new Mail("Jan", "Stan", "Czesc", false);
        Mail mail2 = new Mail("Stan", "Jan", "Pa pa", false);

        //When
        user.getMailbox().add(mail1);
        user.getMailbox().add(mail2);

        //Then
        assertThat(user.getMailbox().get(0)).isEqualTo(mail1);
        assertThat(user.getMailbox().get(1)).isEqualTo(mail2);
    }
}
