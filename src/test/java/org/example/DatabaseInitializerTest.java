package org.example;

import org.example.sqlLiteDbTables.DatabaseInitializer;
import org.jooq.DSLContext;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class DatabaseInitializerTest {
    @Test
    public void shouldInitializeDatabase_WithoutErrors() {
        //Given
        DSLContext mockContext = mock(DSLContext.class);
        DatabaseInitializer databaseInitializer = new DatabaseInitializer(mockContext);

        //Then
        assertNotNull(databaseInitializer);
    }

    @Test
    public void shouldCreateTables(){
        //Given
        DatabaseInitializer mockDatabaseInitializer = mock(DatabaseInitializer.class);

        //When
        mockDatabaseInitializer.createTables();

        //Then
        verify(mockDatabaseInitializer, times(1)).createTables();
    }

    @Test
    public void shouldAddAminToDatabaseIfNotExists(){
        //Given
        DatabaseInitializer mockDatabaseInitializer = mock(DatabaseInitializer.class);

        //When
        mockDatabaseInitializer.addAdminToUsersTable();

        //Then
        verify(mockDatabaseInitializer, times(1)).addAdminToUsersTable();

    }


}
