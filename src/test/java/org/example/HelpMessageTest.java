package org.example;

import org.example.entities.HelpMessage;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class HelpMessageTest {

    @Test
    void shouldCreateHelpMessageWithDefaultConstructor(){
        //When
        HelpMessage helpMessage = new HelpMessage();

        //Then
        assertThat(helpMessage).isNotNull();
        assertThat(helpMessage.getKey()).isNull();
        assertThat(helpMessage.getValue()).isNull();
    }

    @Test
    void shouldCreateHelpMessageWithAllArgsConstructor(){
        //Given
        String testKey = "testKey";
        String testValue = "This is a test message";

        //When
        HelpMessage helpMessage = new HelpMessage(testKey, testValue);

        //Then
        assertThat(helpMessage).isNotNull();
        assertThat(helpMessage.getKey()).isEqualTo(testKey);
        assertThat(helpMessage.getValue()).isEqualTo(testValue);
    }

    @Test
    void shouldGetAndSetKey(){
        //Given
        HelpMessage helpMessage = new HelpMessage();
        String testKey = "testKey";

        //When
        helpMessage.setKey(testKey);

        //Then
        assertThat(helpMessage.getKey()).isEqualTo(testKey);
    }

    @Test
    void shouldGetAndSetValue(){
        //Given
        HelpMessage helpMessage = new HelpMessage();
        String testValue = "This is a test value";

        //When
        helpMessage.setValue(testValue);

        //Then
        assertThat(helpMessage.getValue()).isEqualTo(testValue);
    }
}
