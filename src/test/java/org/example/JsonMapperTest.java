package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.entities.Mail;
import org.example.serializeAndSend.JsonMapper;
import org.example.entities.User;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class JsonMapperTest {

    private final String stringExample = "test string";
    private final String jsonStringExample = "\"test string\"";
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void shouldReturnStringInJsonFormat() throws JsonProcessingException {
        //When
        String result = JsonMapper.stringToJson(stringExample);

        //Then
        assertThat(result).isEqualTo(jsonStringExample);
    }

    @Test
    void shouldReturnStringFormJson() throws JsonProcessingException {
        //When
        String result = JsonMapper.jsonToString(jsonStringExample);

        //Then
        assertThat(result).isEqualTo(stringExample);
    }

    @Test
    void shouldReturnArrayListOfObjectsInJsonFormat() throws JsonProcessingException {
        //Given
        List<Mail> mailList = new ArrayList<>();
        mailList.add(new Mail("sender1", "recipient1", "body1", false));
        mailList.add(new Mail("sender2", "recipient2", "body2", false));

        //When
        String arrayListJson = JsonMapper.arrayListToJson(mailList);

        //Then
        JsonNode arrayNode = objectMapper.readTree(arrayListJson);

        assertThat(arrayNode).isNotNull();
        assertThat(arrayNode).isNotEmpty();
        assertThat(arrayNode.isArray()).isTrue();
        assertThat(arrayNode.size()).isEqualTo(2);

        //Check every item in jsonArray
        for (JsonNode node : arrayNode) {
            assertThat(node.get("sender")).isNotNull();
            assertThat(node.get("recipient")).isNotNull();
            assertThat(node.get("body")).isNotNull();
            assertThat(node.get("read")).isNotNull();
        }
    }

    @Test
    void shouldReturnUserObjectFromJson() throws JsonProcessingException {
        //Given
        String jsonAdmin = """
                  {
                    "type": "User",
                    "login": "Admin",
                    "password": "a",
                    "role": "ADMIN"
                  }
                """;

        //When
        User result = JsonMapper.jsonToObject(jsonAdmin, User.class);

        //Then
        assertThat(result).isNotNull();
        assertThat(result.getType()).isEqualTo("User");
        assertThat(result.getLogin()).isEqualTo("Admin");
        assertThat(result.getPassword()).isEqualTo("a");
        assertThat(result.getRole()).isEqualTo(User.Role.ADMIN);
        assertThat(result.getMailbox()).isEmpty();
    }
}