package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.example.entities.Mail;
import org.example.serializeAndSend.ClientPrintWriterService;
import org.example.serializeAndSend.JsonMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

public class ClientPrintWriterServiceTest {
    private StringWriter stringWriter;
    private ClientPrintWriterService clientPrintWriterService;

    @BeforeEach
    void Setup() {
        stringWriter = new StringWriter();
        clientPrintWriterService = new ClientPrintWriterService(new PrintWriter(stringWriter));
    }

    @Test
    void serializeAndSendString() throws JsonProcessingException {
        //Given
        String message = "testMessage";
        String jsonMessage = JsonMapper.stringToJson(message);

        //When
        clientPrintWriterService.serializeAndSend(message);

        //Then
        String expected = jsonMessage.length() + "\r\n" + jsonMessage;
        assertThat(expected).isEqualTo(stringWriter.toString().trim());
    }

    @Test
    void serializeAndSendStringAndObject() throws JsonProcessingException {
        //Given
        String message = "testMessage";
        String jsonMessage = JsonMapper.stringToJson(message);
        Mail mail = new Mail("sender", "receiver", "body", false);
        String jsonMail = JsonMapper.objectToJson(mail);

        //When
        clientPrintWriterService.serializeAndSend(message, mail);

        //Then
        String expected = jsonMessage.length() + "\r\n" + jsonMessage + jsonMail.length() + "\r\n" + jsonMail;
        assertThat(expected).isEqualTo(stringWriter.toString().trim());
    }

    @Test
    void serializeAndSendArrayList() throws JsonProcessingException {
        //Given
        ArrayList<Mail> mailArray = new ArrayList<>();
        mailArray.add(new Mail("sender1", "receiver1", "body1", false));
        mailArray.add(new Mail("sender2", "sender2", "body2", false));
        String jsonMailArray = JsonMapper.arrayListToJson(mailArray);

        //When
        clientPrintWriterService.serializeAndSend(mailArray);

        //Then
        String expected = jsonMailArray.length() + "\r\n" + jsonMailArray;
        assertThat(expected).isEqualTo(stringWriter.toString().trim());
    }
}
