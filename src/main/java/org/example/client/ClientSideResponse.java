package org.example.client;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.example.entities.HelpMessage;
import org.example.entities.Mail;
import org.example.serializeAndSend.JsonMapper;
import org.example.entities.User;
import org.example.utilities.Constants;

import java.io.BufferedReader;
import java.io.IOException;

@Slf4j
@UtilityClass
public class ClientSideResponse {
    private final BufferedReader INSTREAM = Client.getInStream();

    public static String getResponseFromServer() throws IOException {
        String headerLength = INSTREAM.readLine();
        int messageLength = Integer.parseInt(headerLength);

        char[] buffer = new char[messageLength];
        int readChars = INSTREAM.read(buffer, 0, messageLength);
        return new String(buffer, 0, readChars);
    }

    public void loggOutResponseFromServer(String response) throws IOException {

        JsonNode node = Constants.OBJECT_MAPPER.readTree(response);

        switch (node.getNodeType()) {
            case STRING:
                System.out.println(node.asText());
                break;

            case ARRAY:
                int counter = 1;

                for (JsonNode item : node) {
                    String itemType = JsonMapper.getJsonType(item);

                    switch (itemType) {
                        case "HelpMessage":
                            HelpMessage helpMessage = Constants.OBJECT_MAPPER.treeToValue(item, HelpMessage.class);
                            System.out.println("Command: " + helpMessage.getKey() + " - " + helpMessage.getValue());
                            break;
                        case "Mail":
                            Mail eMail = Constants.OBJECT_MAPPER.treeToValue(item, Mail.class);
                            System.out.println("Nadawca: ### " + eMail.getSender() + " ###");
                            System.out.println("Treść: '" + eMail.getBody() + "'");
                            break;
                        case "User":
                            User user = Constants.OBJECT_MAPPER.treeToValue(item, User.class);
                            System.out.println(counter + ". User login: " + user.getLogin() + ".");
                            counter++;
                            break;
                        default:
                            log.info("Unknown type: " + itemType);
                            break;
                    }
                }
                break;

            case OBJECT:
                System.out.println("got object");
                log.info("got object");
                break;

            default:
                log.info("wrong data type");
        }
    }
}
