package org.example.client;

import org.example.entities.service.MailService;
import org.example.serializeAndSend.ClientPrintWriterService;
import org.example.entities.service.UserService;
import org.example.utilities.Constants;

import java.io.IOException;

public class ClientSideRequest {

    private final ClientPrintWriterService clientPrintWriterService;

    public ClientSideRequest(ClientPrintWriterService printWriter) {
        this.clientPrintWriterService = printWriter;
    }

    public boolean sendRequestToServer(String message) throws IOException {

        MailService mailService = new MailService(Constants.INPUTSCANNER);
        UserService userService = new UserService(clientPrintWriterService);
        String currentUser = userService.getCurrentUserFromServer();

        // This switch handles different client commands.
        // Returns true if a response from the server is expected, false otherwise.
        switch (message.toLowerCase()) {
            case "uptime", "info", "help":
                //This commands always expects response from server.
                clientPrintWriterService.serializeAndSend(message);
                return true;

            case "create":
                if (userService.isAdminLogged()) {
                    clientPrintWriterService.serializeAndSend(message, UserService.getCredentials());
                    return true;
                } else {
                    return false;
                }

            case "login":
                clientPrintWriterService.serializeAndSend(message, UserService.getCredentials());
                return true;

            case "user":
                userService.isUserLogged();
                System.out.println("Aktualnie zalogowany użytkownik: " + currentUser + ".");
                return false;

            case "users":
                if (userService.isAdminLogged()) {
                    clientPrintWriterService.serializeAndSend(message);
                    return true;
                } else {
                    return false;
                }

            case "delete":
                if (userService.isAdminLogged()) {
                    clientPrintWriterService.serializeAndSend(message, UserService.getUserLoginToDelete());
                    return true;
                } else {
                    return false;
                }

            case "send mail":
                if (userService.isUserLogged()) {
                    clientPrintWriterService.serializeAndSend(message, mailService.createMailFromUserInput(currentUser));
                    return true;
                } else {
                    return false;
                }

            case "read mail", "empty mailbox":
                if (userService.isUserLogged()) {
                    clientPrintWriterService.serializeAndSend(message);
                    return true;
                } else {
                    return false;
                }

            default:
                System.out.println("Niewłaściwa komenda");
                return false;
        }
    }
}
