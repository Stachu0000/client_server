package org.example.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.example.serializeAndSend.ClientPrintWriterService;
import org.example.serializeAndSend.JsonMapper;
import org.example.utilities.CloseableUtil;
import org.example.utilities.Constants;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import static org.example.utilities.Constants.*;

@Slf4j
public class Client {
    private Socket clientSocket;
    @Getter
    private static PrintWriter outStream;
    @Getter
    private static BufferedReader inStream;
    private ClientPrintWriterService clientPrintWriterService;

    public static void main(String[] args) {
        Client client = new Client();
        client.startClient(Constants.SERVER_IP, Constants.SERVER_PORT);
    }

    private void startClient(String ip, int port) {

        try {
            clientSocket = new Socket(ip, port);
            outStream = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(), StandardCharsets.UTF_8), true);
            inStream = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), StandardCharsets.UTF_8));

            String initialServerResponse = ClientSideResponse.getResponseFromServer();
            System.out.println(initialServerResponse);

            clientPrintWriterService = new ClientPrintWriterService(outStream);
            ClientSideRequest clientSideRequest = new ClientSideRequest(clientPrintWriterService);

            System.out.println("Type 'help' for available commands.");

            boolean connectionToServer = true;
            while (connectionToServer) {

                String userInput = INPUTSCANNER.nextLine();

                if (clientFinishConnectionToServer(userInput)) {
                    connectionToServer = false;
                } else {
                    boolean messageWasSent = clientSideRequest.sendRequestToServer(userInput);
                    if (messageWasSent) {
                        String response = ClientSideResponse.getResponseFromServer();
                        ClientSideResponse.loggOutResponseFromServer(response);
                    }
                }

            }
        } catch (IOException e) {
            System.out.println("Error connecting to server");
        } finally {
            CloseableUtil.quietlyClose(outStream);
            CloseableUtil.quietlyClose(inStream);
            CloseableUtil.quietlyClose(clientSocket);

        }
    }

    private boolean clientFinishConnectionToServer(String message) throws JsonProcessingException {
        if (message.equals("stop")) {
            String messageJson = JsonMapper.stringToJson(message);
            clientPrintWriterService.printHeaderAndMessage(messageJson);

            log.info("Disconnected from server");
            return true;
        }
        return false;
    }
}