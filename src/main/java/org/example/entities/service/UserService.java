package org.example.entities.service;

import com.fasterxml.jackson.databind.JsonNode;
import org.example.client.ClientSideResponse;
import org.example.entities.service.Credentials;
import org.example.serializeAndSend.ClientPrintWriterService;

import java.io.IOException;

import static org.example.utilities.Constants.*;

public class UserService {

    private final ClientPrintWriterService clientPrintWriterService;

    public UserService(ClientPrintWriterService clientPrintWriterService) {
        this.clientPrintWriterService = clientPrintWriterService;
    }

    public String getCurrentUserFromServer() throws IOException {
        clientPrintWriterService.serializeAndSend("getcurrentuser");
        String response = ClientSideResponse.getResponseFromServer();

        JsonNode node = OBJECT_MAPPER.readTree(response);

        return node.asText();
    }
    public static Credentials getCredentials() {
        System.out.println("Podaj login");
        String login = INPUTSCANNER.nextLine();
        System.out.println("Podaj hasło");
        String password = INPUTSCANNER.nextLine();

        return new Credentials(login, password);
    }

    public static String getUserLoginToDelete() {
        System.out.println("Podaj login użytkownika do usunięcia z bazy danych");
        return INPUTSCANNER.nextLine();
    }

    public boolean isUserLogged() throws IOException {
        String currentUser = getCurrentUserFromServer();

        if(currentUser.equals("not logged in")){
            System.out.println("Obecnie nie jesteś zalogowany.");
            return false;
        }
        else{
            return true;
        }
    }

    public boolean isAdminLogged() throws IOException {

        if(getCurrentUserFromServer().equals("Admin")){
            return true;
        }
        else{
            System.out.println("Nie posiadasz uprawnień administratora.");
            return false;
        }
    }
}

