package org.example.entities.service;

import lombok.extern.slf4j.Slf4j;
import org.example.database.repository.mailServiceRepository.MailRepositoryGet;
import org.example.entities.Mail;

import java.util.Scanner;

@Slf4j
public class MailService {

    private final Scanner inputScanner;

    public MailService(Scanner inputScanner) {
        this.inputScanner = inputScanner;
    }

    public Mail createMailFromUserInput(String sender) {
        String recipientLogin = getRecipientLogin();
        String mailBody = getMailBody();
        return new Mail(sender, recipientLogin, mailBody, false);
    }

    private String getRecipientLogin(){
        System.out.println("Podaj login adresata e-maila.");
        return inputScanner.nextLine();
    }

    private String getMailBody(){
        String body = "";
        boolean validMailLength = false;
        while (!validMailLength) {
            System.out.println("Podaj treść e-maila. Maksymalnie 255 znaków");
            body = inputScanner.nextLine();
            validMailLength = validateMailLength(body);
        }
        return body;
    }

    public static boolean validateMailLength(String body) {
        if (body.length() <= 255) {
            return true;
        } else {
            System.out.println("Podana długość e-maila przkracza 255 znaków.");
            return false;
        }
    }

    public boolean isMailBoxOverloaded(int userId, MailRepositoryGet mailRepositoryGet) {
        int unreadMails = mailRepositoryGet.getNumberOfUnreadMailsForUser(userId);
        if(unreadMails >= 5){
            return true;
        }
        else {
            return false;
        }
    }
}
