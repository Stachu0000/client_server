package org.example.entities;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@NoArgsConstructor
//@JsonTypeInfo(use = Id.NAME, property = "type")
public class HelpMessage {
    private String key;
    private String value;
    private String type;

    public HelpMessage(String key, String value) {
        this.key = key;
        this.value = value;
        this.type = "HelpMessage";
    }
}
