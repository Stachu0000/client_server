package org.example.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.entities.Mail;

import java.util.ArrayList;

@Setter
@Getter
@NoArgsConstructor
public class User{
    private String type;
    private String login;
    private String password;
    private Role role;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<Mail> mailbox = new ArrayList<>();

    public User(String login, String password, Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
        this.type = "User";
    }

    public enum Role {
        ADMIN,
        USER
    }
}
