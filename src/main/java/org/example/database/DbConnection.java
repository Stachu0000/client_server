package org.example.database;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.example.utilities.Constants.*;

@Slf4j
public class DbConnection {

    private static DbConnection instance;
    private Connection connection;
    @Getter
    private DSLContext context;


    private DbConnection() throws SQLException {
        connectToDatabase();
    }

    public static DbConnection getInstance() throws SQLException {
        if (instance == null) {
            instance = new DbConnection();
        }
        return instance;
    }

    public void connectToDatabase() throws SQLException {
        connection = DriverManager
                .getConnection(DATABASE_URL);

        context = DSL.using(connection, SQLDialect.SQLITE);
    }


    public void closeDatabaseConnection() {

        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            log.error("Database close connection error: {}", e.getMessage());
        }
    }
}

