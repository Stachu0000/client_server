package org.example.database.repository.userRepository;

import lombok.extern.slf4j.Slf4j;
import org.example.database.queries.DbUserQueries;
import org.example.database.requests.DbUserRequests;
import org.example.entities.service.Credentials;
import org.example.entities.User;
import org.jooq.DSLContext;

import java.sql.SQLException;

@Slf4j
public class UserRepositorySend {
    private final DbUserRequests DB_USER_REQUESTS;
    private final DbUserQueries DB_USER_QUERIES;

    public UserRepositorySend(DSLContext context) {
        this.DB_USER_REQUESTS = new DbUserRequests(context);
        this.DB_USER_QUERIES = new DbUserQueries(context);
    }

    public void createUser(Credentials credentials) {
        User newUser = new User(credentials.getLogin(), credentials.getPassword(), User.Role.USER);
        DB_USER_REQUESTS.addUserToTableRequest(newUser);
    }

    public boolean deleteUser(String userLogin) {
        Integer userId = DB_USER_QUERIES.getUserIdByLoginQuery(userLogin);

        if (userId != null) {
            DB_USER_REQUESTS.removeUserFromTableRequest(userId);
            return true;
        } else {
            return false;
        }
    }
}
