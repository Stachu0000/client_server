package org.example.database.repository.userRepository;

import lombok.extern.slf4j.Slf4j;
import org.example.database.queries.DbUserQueries;
import org.example.entities.service.Credentials;
import org.example.entities.User;
import org.jooq.DSLContext;

import java.sql.SQLException;
import java.util.List;

@Slf4j
public class UserRepositoryGet {
    private final DbUserQueries DB_USER_QUERIES;

    public UserRepositoryGet(DSLContext context) {
        this.DB_USER_QUERIES = new DbUserQueries(context);
    }
    public List<User> getAllUsersFormDB() {
        return DB_USER_QUERIES.getAllUsersFromDatabaseQuery();
    }

    public boolean isUserInDb(String userLogin) {
        Integer userId = DB_USER_QUERIES.getUserIdByLoginQuery(userLogin);
        return userId != null;
    }

    public Integer getUserIdByLogin(String userLogin) {
        return DB_USER_QUERIES.getUserIdByLoginQuery(userLogin);
    }

    public boolean checkLoginCredentials(Credentials credentials) {
        String password = DB_USER_QUERIES.getUserPasswordByLoginQuery(credentials.getLogin());
        return password != null && password.equals(credentials.getPassword());
    }
}
