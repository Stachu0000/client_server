package org.example.database.repository.mailServiceRepository;

import lombok.extern.slf4j.Slf4j;
import org.example.database.queries.DbMailQueries;
import org.example.entities.Mail;
import org.jooq.DSLContext;

import java.sql.SQLException;
import java.util.List;

@Slf4j
public class MailRepositoryGet {
    private final DbMailQueries DB_MAIL_QUERIES;

    public MailRepositoryGet(DSLContext context) {
        this.DB_MAIL_QUERIES = new DbMailQueries(context);
    }
    public List<Mail> getAllMailByUserId(Integer userid) {
        return DB_MAIL_QUERIES.getAllMailByUserIdQuery(userid);
    }

    public int getNumberOfUnreadMailsForUser(int userId) {
        return DB_MAIL_QUERIES.countUnreadMailsForUserQuery(userId);
    }
}
