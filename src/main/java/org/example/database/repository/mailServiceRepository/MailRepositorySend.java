package org.example.database.repository.mailServiceRepository;

import lombok.extern.slf4j.Slf4j;
import org.example.database.requests.DbMailRequests;
import org.example.entities.Mail;
import org.jooq.DSLContext;

@Slf4j
public class MailRepositorySend {
    private final DbMailRequests DB_MAIL_REQUESTS;

    public MailRepositorySend(DSLContext context) {
        this.DB_MAIL_REQUESTS = new DbMailRequests(context);
    }

    public void addMailToUser(Mail mail, Integer recipientId) {
        DB_MAIL_REQUESTS.addMailToTableRequest(mail, recipientId);
    }

    public void deleteAllMailsByUserId(int userId) {
        DB_MAIL_REQUESTS.deleteAllEmailsByUserIdRequest(userId);
    }

    public void setAllEmailsAsReadByUserId(int userId) {
        DB_MAIL_REQUESTS.setAllEmailsAsReadByUserIdRequest(userId);
    }
}
