package org.example.database.repository.helpMessageRepository;

import lombok.extern.slf4j.Slf4j;
import org.example.database.queries.DbHelpMessageQueries;
import org.example.entities.HelpMessage;
import org.jooq.DSLContext;

import java.sql.SQLException;
import java.util.List;

@Slf4j
public class HelpMessageRepositoryGet {
    private final DbHelpMessageQueries DB_HELP_MESSAGE_QUERIES;

    public HelpMessageRepositoryGet(DSLContext context) {
        this.DB_HELP_MESSAGE_QUERIES = new DbHelpMessageQueries(context);
    }

    public List<HelpMessage> getAllHelpMessagesFromDB() {
        return DB_HELP_MESSAGE_QUERIES.getHelpMessageListQuery();
    }
}
