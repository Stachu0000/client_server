package org.example.database.queries;

import org.example.database.DbConnection;
import org.example.entities.User;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;

import java.util.ArrayList;
import java.util.List;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

public class DbUserQueries {
    private final DSLContext CONTEXT;

    public DbUserQueries(DSLContext context) {
        CONTEXT = context;
    }

    public List<User> getAllUsersFromDatabaseQuery(){
        Result<Record> result = CONTEXT
                .select()
                .from(table("users"))
                .fetch();

        List<User> users = new ArrayList<>();

        for(Record record : result){
            User user = new User(
                    record.get(field("login"), String.class),
                    record.get(field("password"), String.class),
                    User.Role.valueOf(record.get(field("user_role"), String.class))
            );

            users.add(user);
        }
        return users;
    }

    public Integer getUserIdByLoginQuery(String login){
            Record record = CONTEXT
                    .select(field("id"))
                    .from(table("users"))
                    .where(field("login").eq(login))
                    .fetchOne();

            return record != null ? record.get(field("id", Integer.class)) : null;
    }

    public String getUserPasswordByLoginQuery(String login){
        Record record = CONTEXT
                .select(field("password"))
                .from(table("users"))
                .where(field("login").eq(login))
                .fetchOne();

        return record != null ? record.get(field("password"), String.class) : null;
    }
}
