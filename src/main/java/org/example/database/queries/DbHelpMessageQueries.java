package org.example.database.queries;

import org.example.entities.HelpMessage;
import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.Record;

import java.util.ArrayList;
import java.util.List;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

public class DbHelpMessageQueries {
    private final DSLContext CONTEXT;

    public DbHelpMessageQueries(DSLContext context) {
        CONTEXT = context;
    }

    public List<HelpMessage> getHelpMessageListQuery(){
        Result<Record> result = CONTEXT
                .select()
                .from(table("help_messages"))
                .orderBy(field("id").asc())
                .fetch();

        List<HelpMessage> helpMessageList = new ArrayList<>();

        for(Record record : result){
            helpMessageList.add(new HelpMessage(
                    record.get("key", String.class),
                    record.get("value", String.class)
            ));
        }

        return helpMessageList;
    }
}
