package org.example.database.queries;

import org.example.entities.Mail;
import org.jooq.DSLContext;
import org.jooq.Result;

import java.util.ArrayList;
import java.util.List;

import org.jooq.Record;

import static org.jooq.impl.DSL.*;

public class DbMailQueries {

    private final DSLContext CONTEXT;
    public DbMailQueries(DSLContext context) {
        CONTEXT = context;
    }

    public List<Mail> getAllMailByUserIdQuery(Integer userId){
        Result<Record> result = CONTEXT
                .select()
                .from(table("mails"))
                .where(field("user_id").eq(userId))
                .fetch();

        List<Mail> mails = new ArrayList<>();

        for(Record record : result){
            Mail mail = new Mail(
                    record.get(field("sender"), String.class),
                    record.get(field("recipient"), String.class),
                    record.get(field("body"), String.class),
                    record.get(field("is_read"), Boolean.class)
            );

            mails.add(mail);
        }

        return mails;
    }

    public Integer countUnreadMailsForUserQuery(int userId){
        return  CONTEXT
                .selectCount()
                .from(table("mails"))
                .where(field("is_read").eq(false))
                .and(field("user_id").eq(userId))
                .fetchOne(0, Integer.class);
    }
}
