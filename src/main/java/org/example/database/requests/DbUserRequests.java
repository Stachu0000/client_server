package org.example.database.requests;

import org.example.entities.User;
import org.jooq.DSLContext;

import static org.jooq.impl.DSL.*;

public class DbUserRequests {
    private final DSLContext CONTEXT;

    public DbUserRequests(DSLContext context) {
        CONTEXT = context;
    }

    public void addUserToTableRequest(User user){

        CONTEXT.insertInto(table("users"))
                    .set(field("login"), user.getLogin())
                    .set(field("password"), user.getPassword())
                    .set(field("user_role"), user.getRole().toString())
                    .execute();

    }

    public void removeUserFromTableRequest(int userId){
        CONTEXT.deleteFrom(table("users"))
                .where(field("id").eq(userId))
                .execute();
    }
}
