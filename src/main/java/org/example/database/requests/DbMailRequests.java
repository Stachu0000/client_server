package org.example.database.requests;

import org.example.entities.Mail;
import org.jooq.DSLContext;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

public class DbMailRequests {

    private final DSLContext CONTEXT;

    public DbMailRequests(DSLContext context) {
        CONTEXT = context;
    }

    public void addMailToTableRequest(Mail mail, Integer recipientId){
        CONTEXT.insertInto(table("mails"))
                .set(field("sender"), mail.getSender())
                .set(field("recipient"), mail.getRecipient())
                .set(field("body"), mail.getBody())
                .set(field("user_id"), recipientId)
                .execute();
    }

    public void setAllEmailsAsReadByUserIdRequest(int userId){
        CONTEXT.update(table("mails"))
                .set(field("is_read"), true)
                .where(field("user_id").eq(userId))
                .execute();
    }

    public void deleteAllEmailsByUserIdRequest(int userId){
        CONTEXT.deleteFrom(table("mails"))
                .where(field("user_id").eq(userId))
                .execute();
    }
}
