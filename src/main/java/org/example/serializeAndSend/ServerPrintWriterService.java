package org.example.serializeAndSend;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.experimental.UtilityClass;
import org.example.server.Server;

import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

@UtilityClass
public class ServerPrintWriterService {

    private final PrintWriter OUTSTREAM = Server.getOutStream();
    public void serializeAndSend(String message) throws JsonProcessingException {
        serializeStringToJsonAndSend(message);
    }

    public <T> void serializeAndSend(String message, T obj) throws JsonProcessingException {
        serializeStringToJsonAndSend(message);
        serializeObjectToJsonAndSend(obj);
    }

    public void serializeAndSend(String firstMessage, String secondMessage) throws JsonProcessingException {
        serializeStringToJsonAndSend(firstMessage);
        serializeStringToJsonAndSend(secondMessage);
    }

    public <T> void serializeAndSend(List<T> arrayList) throws JsonProcessingException {
        serializeArrayListToJsonAndSend(arrayList);
    }

    public <T> void serializeAndSend(T obj) throws JsonProcessingException {
        serializeObjectToJsonAndSend(obj);
    }

    public void printHeaderAndMessage(String messageJson) {
        int messageLength = messageJson.getBytes(StandardCharsets.UTF_8).length;
        OUTSTREAM.println(messageLength);
        OUTSTREAM.print(messageJson);
        OUTSTREAM.flush();
    }

    public void serializeStringToJsonAndSend(String message) throws JsonProcessingException {
        String messageJson = JsonMapper.stringToJson(message);
        ServerPrintWriterService.printHeaderAndMessage(messageJson);
    }

    public <T> void serializeObjectToJsonAndSend(T obj) throws JsonProcessingException {
        String requestJson = JsonMapper.objectToJson(obj);
        ServerPrintWriterService.printHeaderAndMessage(requestJson);
    }

    public <T> void serializeArrayListToJsonAndSend(List<T> arrayList) throws JsonProcessingException {
        String requestJson = JsonMapper.arrayListToJson(arrayList);
        ServerPrintWriterService.printHeaderAndMessage(requestJson);
    }
}