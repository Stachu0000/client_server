package org.example.serializeAndSend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import lombok.experimental.UtilityClass;
import static org.example.utilities.Constants.*;
import java.util.List;


@UtilityClass
public class JsonMapper {

    public String stringToJson(String text) throws JsonProcessingException {
        return OBJECT_MAPPER.writeValueAsString(text);
    }

    public String jsonToString(String json) throws JsonProcessingException {
        return OBJECT_MAPPER.readValue(json, String.class);
    }

    public <T> String arrayListToJson(List<T> list) throws JsonProcessingException {
        ArrayNode arrayNode = OBJECT_MAPPER.createArrayNode();
        for (T item : list) {
            String json = objectToJson(item);

            JsonNode node = OBJECT_MAPPER.readTree(json);

            arrayNode.add(node);
        }
        return OBJECT_MAPPER.writeValueAsString(arrayNode);
    }

    public String jsonNodeToString(JsonNode jsonNode){
        return OBJECT_MAPPER.convertValue(jsonNode, new TypeReference<>() {
        });
    }

    public <T> String objectToJson(T obj) throws JsonProcessingException {
        return OBJECT_MAPPER.writeValueAsString(obj);
    }

    public <T> T jsonToObject(String json, Class<T> TClass) throws JsonProcessingException {
        return OBJECT_MAPPER.readValue(json, TClass);
    }

    public String getJsonType(JsonNode node) throws JsonProcessingException{
        if (!node.has("type")) throw new JsonProcessingException("Node does not contain 'type' field"){};

        return node.get("type").asText();
    }
}
