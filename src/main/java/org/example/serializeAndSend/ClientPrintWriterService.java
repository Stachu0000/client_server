package org.example.serializeAndSend;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.experimental.UtilityClass;
import org.example.client.Client;

import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class ClientPrintWriterService {
    private final PrintWriter outStream;

    public ClientPrintWriterService(PrintWriter outStream) {
        this.outStream = outStream;
    }

    public void serializeAndSend(String message) throws JsonProcessingException {
        serializeStringToJsonAndSend(message);
    }

    public <T> void serializeAndSend(String message, T obj) throws JsonProcessingException {
        serializeStringToJsonAndSend(message);
        serializeObjectToJsonAndSend(obj);
    }

    public void serializeAndSend(String firstMessage, String secondMessage) throws JsonProcessingException {
        serializeStringToJsonAndSend(firstMessage);
        serializeStringToJsonAndSend(secondMessage);
    }

    public <T> void serializeAndSend(List<T> arrayList) throws JsonProcessingException {
        serializeArrayListToJsonAndSend(arrayList);
    }

    public void printHeaderAndMessage(String messageJson) {
        int messageLength = messageJson.getBytes(StandardCharsets.UTF_8).length;
        outStream.println(messageLength);
        outStream.print(messageJson);
        outStream.flush();
    }

    public void serializeStringToJsonAndSend(String message) throws JsonProcessingException {
        String messageJson = JsonMapper.stringToJson(message);
        printHeaderAndMessage(messageJson);
    }

    public <T> void serializeObjectToJsonAndSend(T obj) throws JsonProcessingException {
        String requestJson = JsonMapper.objectToJson(obj);
        printHeaderAndMessage(requestJson);
    }

    public <T> void serializeArrayListToJsonAndSend(List<T> arrayList) throws JsonProcessingException {
        String requestJson = JsonMapper.arrayListToJson(arrayList);
        printHeaderAndMessage(requestJson);
    }
}
