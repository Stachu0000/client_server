package org.example.sqlLiteDbTables;

import org.example.database.DbConnection;
import org.jooq.DSLContext;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;
import org.jooq.Record;

import java.sql.SQLException;

public class DatabaseInitializer {
    private final DSLContext CONTEXT;
    private final UsersTable usersTable;
    private final MailsTable mailsTable;
    private final HelpMessagesTable helpMessagesTable;

    public DatabaseInitializer(DSLContext context) {
        this.CONTEXT = context;
        enableForeignKeys();
        this.usersTable = new UsersTable(CONTEXT);
        this.mailsTable = new MailsTable(CONTEXT);
        this.helpMessagesTable = new HelpMessagesTable(CONTEXT);
    }

    public void createTables(){
        usersTable.createUsersTable();
        mailsTable.createMailsTable();
        helpMessagesTable.createHelpMessagesTable();
        helpMessagesTable.addRecordsToTable();
    }

    public void addAdminToUsersTable() {

        if(!isAdminInDatabase()){
            CONTEXT.insertInto(table("users"))
                    .set(field("login"), "Admin")
                    .set(field("password"), "a")
                    .set(field("user_role"), "ADMIN")
                    .execute();
        }
    }

    private boolean isAdminInDatabase(){
        Record record = CONTEXT.select()
                .from(table("users"))
                .where(field("id").eq(1))
                .and(field("login").eq("Admin"))
                .fetchOne();

        return record != null;
    }

    private void enableForeignKeys(){
        CONTEXT.execute("PRAGMA foreign_keys = ON;");
    }
}
