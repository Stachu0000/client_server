package org.example.sqlLiteDbTables;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;

import static org.jooq.impl.SQLDataType.*;
public class MailsTable {
    private final DSLContext context;

    public MailsTable(DSLContext context) {
        this.context = context;
    }

    public void createMailsTable() {
        context.createTableIfNotExists("mails")
                .column("id", INTEGER.identity(true))
                .column("sender", VARCHAR(100).nullable(false))
                .column("recipient", VARCHAR(100).nullable(false))
                .column("body", VARCHAR(255))
                .column("created_at", TIMESTAMP.defaultValue(DSL.currentTimestamp()))
                .column("user_id", INTEGER)
                .column("is_read", BOOLEAN.defaultValue(false))
                .constraints(
                        DSL.constraint("pk_mails").primaryKey("id"),
                        DSL.foreignKey("user_id").references("users", "id").onDeleteCascade()
                )
                .execute();
    }
}
