package org.example.sqlLiteDbTables;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;

import static org.jooq.impl.DSL.*;
import static org.jooq.impl.SQLDataType.*;

public class HelpMessagesTable {
    private final DSLContext CONTEXT;

    public HelpMessagesTable(DSLContext context) {
        this.CONTEXT = context;
    }

    public void createHelpMessagesTable() {
        CONTEXT.createTableIfNotExists("help_messages")
                .column("id", INTEGER.identity(true))
                .column("key", VARCHAR(255).nullable(false))
                .column("value", VARCHAR(255).nullable(false))
                .constraints(
                        DSL.constraint("pk_help_messages").primaryKey("id")
                )
                .execute();
    }

    public void addRecordsToTable(){
        if(isHelpMessagesTableEmpty()){
            addHelpMessagesToDatabase();
        }
    }
    private boolean isHelpMessagesTableEmpty() {
        Integer count = CONTEXT.select(count())
                .from(table("help_messages"))
                .fetchOne(0, Integer.class);

        return count == 0;
    }

    private void addHelpMessagesToDatabase() {
        CONTEXT.insertInto(table("help_messages"))
                .columns(field("id"), field("key"), field("value"))
                .values(1, "uptime", "Returns server lifespan in seconds.")
                .execute();
        CONTEXT.insertInto(table("help_messages"))
                .columns(field("id"), field("key"), field("value"))
                .values(2, "info", "Returns server version and creation date.")
                .execute();

        CONTEXT.insertInto(table("help_messages"))
                .columns(field("id"), field("key"), field("value"))
                .values(3, "stop", "Breaks connection with server.")
                .execute();

        CONTEXT.insertInto(table("help_messages"))
                .columns(field("id"), field("key"), field("value"))
                .values(4, "create", "Create new account.")
                .execute();

        CONTEXT.insertInto(table("help_messages"))
                .columns(field("id"), field("key"), field("value"))
                .values(5, "delete", "Delete account.")
                .execute();

        CONTEXT.insertInto(table("help_messages"))
                .columns(field("id"), field("key"), field("value"))
                .values(6, "user", "Returns current user login.")
                .execute();

        CONTEXT.insertInto(table("help_messages"))
                .columns(field("id"), field("key"), field("value"))
                .values(7, "users", "Display list of users.")
                .execute();

        CONTEXT.insertInto(table("help_messages"))
                .columns(field("id"), field("key"), field("value"))
                .values(8, "send mail", "Sends new e-mail.")
                .execute();

        CONTEXT.insertInto(table("help_messages"))
                .columns(field("id"), field("key"), field("value"))
                .values(9, "read mail", "Display logged user e-mails.")
                .execute();

        CONTEXT.insertInto(table("help_messages"))
                .columns(field("id"), field("key"), field("value"))
                .values(10, "empty mailbox", "Delete all emails form mailbox.")
                .execute();
    }
}
