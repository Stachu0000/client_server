package org.example.sqlLiteDbTables;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;

import static org.jooq.impl.SQLDataType.*;

public class UsersTable {
    private final DSLContext context;

    public UsersTable(DSLContext context) {
        this.context = context;
    }

    public void createUsersTable() {
        context.createTableIfNotExists("users")
                .column("id", INTEGER.identity(true))
                .column("login", VARCHAR(100).nullable(false))
                .column("password", VARCHAR(100).nullable(false))
                .column("user_role", VARCHAR(100).nullable(false).defaultValue("USER"))
                .column("created_at", TIMESTAMP.nullable(false).defaultValue(DSL.currentTimestamp()))
                .constraints(
                        DSL.constraint("pk_users").primaryKey("id"),
                        DSL.constraint("unique_login").unique("login")
                )
                .execute();
    }
}
