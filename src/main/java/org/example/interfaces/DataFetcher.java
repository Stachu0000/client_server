package org.example.interfaces;

import java.sql.SQLException;

@FunctionalInterface
public interface DataFetcher<T> {
    T fetch();
}
