package org.example.utilities;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Constants {
    //Server related constants
    public static final String SERVER_IP = "localhost";
    public static final int SERVER_PORT = 5000;
    public static final String VERSION = "5.0.0";


    //Json handling
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    //Database
    public static final String DATABASE_URL = "jdbc:sqlite:C:/Java od podstaw/Client_Server/db/client_server_db.db";

    //Input Scanner
    public static final Scanner INPUTSCANNER = new Scanner(System.in, StandardCharsets.UTF_8);
}
