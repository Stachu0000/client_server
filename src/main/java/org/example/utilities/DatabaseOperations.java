package org.example.utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.example.interfaces.DataFetcher;
import org.example.serializeAndSend.ServerPrintWriterService;

import java.sql.SQLException;

@Slf4j
@UtilityClass
public class DatabaseOperations {

    public <T> void fetchDataAndSendToClient(DataFetcher<T> fetcher) throws JsonProcessingException {
        T data = fetcher.fetch();
        ServerPrintWriterService.serializeAndSend(data);
    }
}
