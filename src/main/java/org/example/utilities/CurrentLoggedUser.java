package org.example.utilities;

import lombok.Setter;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CurrentLoggedUser {
    @Setter
    private String currentLoggedUser;


    public String getCurrentLoggedUser() {
        if (currentLoggedUser != null) {
            return currentLoggedUser;
        } else {
            return "not logged in";
        }
    }
}
