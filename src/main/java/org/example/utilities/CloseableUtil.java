package org.example.utilities;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.Closeable;
import java.io.IOException;

@Slf4j
@UtilityClass
public class CloseableUtil {
    public void quietlyClose(Closeable closeable){
        if(closeable != null){
            try {
                closeable.close();
            } catch (IOException e) {
                log.error("Error during closing" + closeable.getClass(), e);
            }
        }
    }
}
