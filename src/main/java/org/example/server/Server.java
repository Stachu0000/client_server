package org.example.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.example.database.DbConnection;
import org.example.sqlLiteDbTables.DatabaseInitializer;
import org.example.utilities.CloseableUtil;
import org.example.utilities.Constants;
import org.jooq.DSLContext;

@Slf4j
public class Server {
    private LocalDateTime setupServerTime;
    @Getter
    private static BufferedReader inStream;
    @Getter
    private static PrintWriter outStream;
    private ServerSocket serverSocket;
    private Socket clientSocket;


    public static void main(String[] args) {
        Server server = new Server();
        server.startServer(Constants.SERVER_PORT);
    }

    private void startServer(int port) {


        try {
            serverSocket = new ServerSocket(port);
            clientSocket = serverSocket.accept();
            outStream = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(), StandardCharsets.UTF_8), true);
            inStream = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), StandardCharsets.UTF_8));

            DSLContext context = getDatabaseContext();

            setupServerTime = LocalDateTime.now();

            initializeSQLiteDatabase(context);
            sendStringMessageToClient("Connected to Server");
            log.info("\n Client connected");

            ServerSideResponse serverSideResponse = new ServerSideResponse();
            boolean isConected = true;

            while (isConected) {
                if (inStream.ready()) {
                    String requestJson = ServerSideRequest.getRequestJson();
                    isConected = serverSideResponse.sendResponseMessage(context, requestJson, setupServerTime, Constants.VERSION);
                }
            }

            log.info("\nClient disconnected");

        } catch (SQLException e) {
            log.error("SQLException cached by server class", e);
            sendStringMessageToClient("Internal server error");
        } catch (IOException e) {
            log.error("Error connection with Client", e);
        } finally {
            CloseableUtil.quietlyClose(outStream);
            CloseableUtil.quietlyClose(inStream);
            CloseableUtil.quietlyClose(clientSocket);
            CloseableUtil.quietlyClose(serverSocket);
        }
    }

    private DSLContext getDatabaseContext() throws SQLException {
        return DbConnection.getInstance().getContext();
    }
    private void initializeSQLiteDatabase(DSLContext context) {
        DatabaseInitializer databaseInitializer = new DatabaseInitializer(context);
        databaseInitializer.createTables();
        databaseInitializer.addAdminToUsersTable();
    }

    private void sendStringMessageToClient(String message) {
        int messageLength = message.length();
        outStream.println(messageLength);
        outStream.print(message);
        outStream.flush();
    }
}
