package org.example.server;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.example.serializeAndSend.ServerPrintWriterService;
import org.example.utilities.CurrentLoggedUser;
import org.jooq.DSLContext;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;

import static org.example.utilities.Constants.*;

@Slf4j
public class ServerSideResponse {
    public boolean sendResponseMessage(DSLContext context, String requestJson, LocalDateTime setupServerTime, String serverVersion) throws IOException {

        ServerCommandService serverCommandService = new ServerCommandService(context);

        JsonNode node = OBJECT_MAPPER.readTree(requestJson);

        switch (node.asText().toLowerCase()) {
            case "stop":
                return false;

            case "uptime":
                serverCommandService.sendServerLifespan(setupServerTime);
                return true;

            case "info":
                serverCommandService.sendServerVersionAndCreationDate(setupServerTime, serverVersion);
                return true;

            case "help":
                serverCommandService.getAllHelpMessagesAndSendToClient();
                return true;

            case "create":
                serverCommandService.getCredentialsFromClientAndCreateNewUser();
                return true;

            case "login":
                serverCommandService.getCredentialsFromClientAndLogin();
                return true;

            case "delete":
                serverCommandService.getUserLoginFromClientAndDelete();
                return true;

            case "send mail":
                serverCommandService.getMailFromClientAndAddToUserFromDB();
                return true;

            case "read mail":
                serverCommandService.readUserMailAndSendToClient();
                return true;

            case "empty mailbox":
                serverCommandService.deleteAllReadUserEmails();
                return true;

            case "users":
                serverCommandService.readAllUsersAndSendToClient();
                return true;

            case "getcurrentuser":
                ServerPrintWriterService.serializeAndSend(CurrentLoggedUser.getCurrentLoggedUser());
                return true;
            default:
                ServerPrintWriterService.serializeAndSend("incorrect message");
                return true;
        }
    }
}
