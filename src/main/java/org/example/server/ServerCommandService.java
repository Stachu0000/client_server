package org.example.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.example.database.repository.helpMessageRepository.HelpMessageRepositoryGet;
import org.example.database.repository.mailServiceRepository.MailRepositoryGet;
import org.example.database.repository.mailServiceRepository.MailRepositorySend;
import org.example.database.repository.userRepository.UserRepositoryGet;
import org.example.database.repository.userRepository.UserRepositorySend;
import org.example.serializeAndSend.ServerPrintWriterService;
import org.example.entities.service.Credentials;
import org.example.entities.Mail;
import org.example.entities.service.MailService;
import org.example.serializeAndSend.JsonMapper;
import org.example.utilities.CurrentLoggedUser;
import org.example.utilities.DatabaseOperations;
import org.jooq.DSLContext;

import java.io.IOException;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.example.utilities.Constants.INPUTSCANNER;

@Slf4j
public class ServerCommandService {

    private final MailService mailService = new MailService(INPUTSCANNER);
    private final UserRepositoryGet userRepositoryGet;
    private final UserRepositorySend userRepositorySend;
    private final MailRepositoryGet mailRepositoryGet;
    private final MailRepositorySend mailRepositorySend;
    private final HelpMessageRepositoryGet helpMessageRepositoryGet;

    public ServerCommandService(DSLContext context) {
        this.userRepositoryGet = new UserRepositoryGet(context);
        this.userRepositorySend = new UserRepositorySend(context);
        this.mailRepositoryGet = new MailRepositoryGet(context);
        this.mailRepositorySend = new MailRepositorySend(context);
        this.helpMessageRepositoryGet = new HelpMessageRepositoryGet(context);
    }

    public void sendServerLifespan(LocalDateTime setupServerTime) throws JsonProcessingException {
        Duration upTime = Duration.between(setupServerTime, LocalDateTime.now());
        String upTimeResponse = "Server lifespan is: " + upTime.toSeconds() + " seconds.";
        String upTimeJson = JsonMapper.stringToJson(upTimeResponse);
        ServerPrintWriterService.serializeAndSend(upTimeJson);
    }

    public void sendServerVersionAndCreationDate(LocalDateTime setupServerTime, String serverVersion) throws JsonProcessingException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String infoResponse = "Server version: " + serverVersion + ". Creation date: " + setupServerTime.format(formatter);
        String infoJson = JsonMapper.stringToJson(infoResponse);
        ServerPrintWriterService.serializeAndSend(infoJson);
    }

    public void getCredentialsFromClientAndCreateNewUser() throws IOException {

        Credentials credentials = ServerSideRequest.getCredentialsFromClient();

        if (!userRepositoryGet.isUserInDb(credentials.getLogin())) {

            userRepositorySend.createUser(credentials);
            ServerPrintWriterService
                    .serializeAndSend("Utworzono konto o logine: " + credentials.getLogin() + " i haśle: " + credentials.getPassword() + ".");

        } else {
            ServerPrintWriterService.serializeAndSend("Użytkownik o podanym loginie już istnieje.");
        }
    }

    public void getCredentialsFromClientAndLogin() throws IOException {

        Credentials credentials = ServerSideRequest.getCredentialsFromClient();

        boolean validatedCredentials = userRepositoryGet.checkLoginCredentials(credentials);

        if (validatedCredentials) {
            CurrentLoggedUser.setCurrentLoggedUser(credentials.getLogin());
            log.info("Zalogowano użytkownika: " + CurrentLoggedUser.getCurrentLoggedUser());
            String loginResponse = "Login success";

            ServerPrintWriterService.serializeAndSend(loginResponse);
        } else {
            ServerPrintWriterService.serializeAndSend("Podano niewłaściwy login lub hasło.");
        }
    }

    public void getUserLoginFromClientAndDelete() throws IOException {
        String userLogin = ServerSideRequest.getMessagegeFromCLientAsString();

        boolean isDeleted = userRepositorySend.deleteUser(userLogin);

        if (isDeleted) {
            ServerPrintWriterService.serializeAndSend("Użytkownik o loginie: '" + userLogin + "' został usunięty.");
        } else {
            ServerPrintWriterService.serializeAndSend("Nie znaleziono użytkownika o podanym loginie.");
        }
    }

    public void getMailFromClientAndAddToUserFromDB() throws IOException {
        Mail mail = ServerSideRequest.getMessageFromClientAsObject(Mail.class);
        Integer recipientId = userRepositoryGet.getUserIdByLogin(mail.getRecipient());

        if (recipientId == null) {
            ServerPrintWriterService.serializeAndSend("Podany odbiorca nie występuje w bazie danych");
        } else {
            boolean isMailBoxOverloaded = mailService.isMailBoxOverloaded(recipientId, mailRepositoryGet);

            if (isMailBoxOverloaded) {
                ServerPrintWriterService.serializeAndSend("Skrzynka odbiorcza adresata jest przepełniona.");
            } else {
                mailRepositorySend.addMailToUser(mail, recipientId);
                ServerPrintWriterService.serializeAndSend(mail.getRecipient() + " otrzymał wiadomość.");
            }
        }
    }

    public void readUserMailAndSendToClient() throws IOException {

        String currentLoggedUser = CurrentLoggedUser.getCurrentLoggedUser();
        int currentUserId = userRepositoryGet.getUserIdByLogin(currentLoggedUser);
        mailRepositorySend.setAllEmailsAsReadByUserId(currentUserId);
        List<Mail> userEmails = mailRepositoryGet.getAllMailByUserId(currentUserId);

        if (userEmails == null || userEmails.isEmpty()) {
            ServerPrintWriterService.serializeAndSend("Skrzynka mailowa jest pusta.");
        } else {
            ServerPrintWriterService.serializeAndSend(userEmails);
        }
    }

    public void readAllUsersAndSendToClient() throws JsonProcessingException {
        DatabaseOperations.fetchDataAndSendToClient(userRepositoryGet::getAllUsersFormDB);
    }

    public void deleteAllReadUserEmails() throws     JsonProcessingException {
        String currentLoggedUser = CurrentLoggedUser.getCurrentLoggedUser();
        int currentUserId = userRepositoryGet.getUserIdByLogin(currentLoggedUser);
        int numberOfUnReadEmails = mailRepositoryGet.getNumberOfUnreadMailsForUser(currentUserId);

        if(numberOfUnReadEmails == 0){
            mailRepositorySend.deleteAllMailsByUserId(currentUserId);
            ServerPrintWriterService.serializeAndSend("Skrzynka pocztowa została wyczyszczona.");
        }
        else{
            ServerPrintWriterService.serializeAndSend("Masz %d nieodczytanych wiadomości. Nie możesz wyczyścić skrzynki mailowej.".formatted(numberOfUnReadEmails));
        }
    }

    public void getAllHelpMessagesAndSendToClient() throws JsonProcessingException {
        DatabaseOperations.fetchDataAndSendToClient(helpMessageRepositoryGet::getAllHelpMessagesFromDB);
    }
}
