package org.example.server;

import lombok.experimental.UtilityClass;
import org.example.entities.service.Credentials;
import org.example.serializeAndSend.JsonMapper;

import java.io.BufferedReader;
import java.io.IOException;

@UtilityClass
public class ServerSideRequest {

    private final BufferedReader INSTREAM = Server.getInStream();

    public String getRequestJson() throws IOException {
        String headerLength = INSTREAM.readLine();
        int messageLength = Integer.parseInt(headerLength);

        char[] buffer = new char[messageLength];
        int readChars = INSTREAM.read(buffer, 0, messageLength);
        return new String(buffer, 0, readChars);
    }

    public Credentials getCredentialsFromClient() throws IOException {
        String createCredentialsJson = ServerSideRequest.getRequestJson();
        return JsonMapper.jsonToObject(createCredentialsJson, Credentials.class);
    }

    public String getMessagegeFromCLientAsString() throws IOException {
        String userLoginJson = ServerSideRequest.getRequestJson();
        return JsonMapper.jsonToString(userLoginJson);
    }

    public <T> T getMessageFromClientAsObject(Class<T> TClass) throws IOException {
        String mailJson = ServerSideRequest.getRequestJson();
        return JsonMapper.jsonToObject(mailJson, TClass);

    }

}
